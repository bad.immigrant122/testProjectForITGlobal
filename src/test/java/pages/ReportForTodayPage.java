package pages;

import io.qameta.allure.Step;
import org.openqa.selenium.By;
import ru.yandex.qatools.htmlelements.element.Button;
import ru.yandex.qatools.htmlelements.element.TextBlock;

import static com.codeborne.selenide.Selenide.$;

public class ReportForTodayPage {

    /******************************************************/
    /********************HTML-ELEMENTS*********************/
    /******************************************************/

    private Button htmlHappyButton= new Button($(By.xpath("//div[@data-original-title='Счастлив']")));
    private Button htmlNeutralButton = new Button($(By.xpath("//div[@data-original-title='Нейтрально']")));
    private Button htmlAngryButton = new Button($(By.xpath("//div[@data-original-title='Злой']")));
    private Button htmlSubmitReport = new Button($(By.xpath("//button[@type='submit']")));
    private TextBlock htmlWarningText = new TextBlock($(By.xpath("//div[@class='modal fade show']//h5[@id='exampleModalLabel']")));
    private Button htmlCancelButton = new Button($(By.xpath("//div[@class ='modal fade show']//button[contains (text(), 'Отмена')]")));

    /******************************************************/
    /************************METHODS***********************/
    /******************************************************/


    public enum Mood{
        Счастлив, Нейтрально, Злой
    }

    @Step("Выбор настроения")
    public void chooseMood (Mood mood){
       $(By.xpath("//div[@data-original-title='"+  mood + "']"));
    }

    @Step("Нажатие на кнопку 'Сохранить отчет'")
    public void submitReport() {
        htmlSubmitReport.click();
    }

    @Step("Взять текст из предупреждения об ошибке")
    public String getTextOfWarning(){
        return htmlWarningText.getText();
    }

    @Step("Нажать на кнопку 'Отмена' для модального окна")
    public void clickCancelButton(){
        htmlCancelButton.click();
    }
}
