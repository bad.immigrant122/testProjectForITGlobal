package configurations;

import org.testng.annotations.DataProvider;

public class DataProviderClass {

    @DataProvider(name = "test-data")
    public static Object[][] dataProviderMethodWithTestData(){
        return new Object[][] {{ "Авто Пользователь", "12345678", "Счастлив"}, { "Авто Мяу", "12345", "Счастлив"}, {"Авто Пользователь", "12345678", "Нейтрально"},
                {"Авто Мяу", "12345", "Нейтрально"}, {"Авто Пользователь", "12345678", "Злой"},  {"Авто Мяу", "12345", "Злой"}  };
    }
}