package autotests.authorized;

import com.codeborne.selenide.WebDriverRunner;
import configurations.ConfProperties;
import io.qameta.allure.Step;
import okhttp3.*;
import org.openqa.selenium.Cookie;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Parameters;

import java.io.IOException;
import java.net.CookieManager;
import java.net.CookiePolicy;

import static com.codeborne.selenide.Selenide.*;

public class AuthorizedREST {

    @Step("Проверка логина через REST API")
    @BeforeMethod
    @Parameters({"login", "password"})
    public void authorizedTest(String login, String password) throws IOException {
        CookieManager cookieManager = new CookieManager();
        cookieManager.setCookiePolicy(CookiePolicy.ACCEPT_ALL);
        JavaNetCookieJar cookieJar = new JavaNetCookieJar(cookieManager);

        ConfProperties confProperties = ConfProperties.getConfProperties();
        String loginPage = confProperties.getProperty("loginPage");
        String loginCheck = confProperties.getProperty("loginCheckPage");

        OkHttpClient client = new OkHttpClient.Builder()
                .cookieJar(cookieJar)
                .followRedirects(false)
                .build();

        Request request = new Request.Builder()
                .url(loginPage)
                .build();

        Call call = client.newCall(request);
        Response responseGet = call.execute();

        RequestBody formBody = new FormBody.Builder()
                .add("_csrf", "")
                .add("_username", login)
                .add("_password", password)
                .add("_submit", "Войти")
                .build();

        Request requestPost = new Request.Builder()
                .url(loginCheck)
                .post(formBody)
                .build();

        Call callPost = client.newCall(requestPost);
        Response responsePost = callPost.execute();

        open(confProperties.getProperty("loginPage"));

        WebDriverRunner.getWebDriver().manage().deleteAllCookies();

        cookieManager.getCookieStore().getCookies().forEach(httpCookie -> {
            Cookie cookie = new Cookie(
                    httpCookie.getName(),
                    httpCookie.getValue(),
                    httpCookie.getDomain(),
                    httpCookie.getPath(),
                    null
            );

            WebDriverRunner.getWebDriver().manage()
                    .addCookie(cookie);
        });
        refresh();
    }


    @Step ("Закрытие браузера")
    @AfterMethod
    public void exit() {
        if (WebDriverRunner.getWebDriver() != null) {
            closeWebDriver();
        }
    }
}
